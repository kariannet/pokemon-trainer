# Pokemon Trainer

## Description

Pokemon Trainer is an application where users can register themselves as a PokéMon trainer and collect PokéMon from the PokéDex page to their own personal collection.

The game can be found [here](https://lit-brook-50713.herokuapp.com/). 

### Landing Page
In Landing the user can register their trainer name and will be redirected to the Catalogue upon clicking the "Continue"-button.

### Catalogue Page
In the Catalogue page the user can see all PokéMon from the PokéAPI. Forty PokéMon are displayed at the time, and the user can navigate through all the PokéMon with the "next" and "previous" buttons. To add a PokéMon to their personal collection, the user must click the "Add"-button.

### Trainer Page
In the trainer page, the user can view their own PokéMon collection. Clicking the "Release"-button will remove the PokéMon from their collection.


## Installation

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.1.
To install Angular CLI using node, run following code in terminal:

```bash
npm install
npm install -g @angular/cli
```
## Usage
Run
```bash
ng serve
```
in terminal for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## API
## Trainer API
The trainer API store all information regarding the registered trainer object: username, userID and their collected PokéMon.

## PokéAPI
The [PokéAPI](https://github.com/PokeAPI) is used to get PokéMon-data (name, id and image) of each PokéMon. 

## Contributing
[Jostein Skadberg](https://gitlab.com/josteinskadberg) and [Karianne Tesaker](https://gitlab.com/kariannet)

## Component Tree
[PokemonTrainerComponentTree.pdf](PokemonTrainerComponentTree.pdf)
