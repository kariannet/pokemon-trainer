import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokedexCollectionService } from 'src/app/services/pokedex-collection.service';
import { SessionService } from 'src/app/services/Session.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css'],
})
export class PokemonListComponent implements OnInit {
  private _pokemons: Pokemon[] | null = [];
  private _isTrainerList: boolean = false;
  private POKE_API_URL = environment.POKE_API_URL
  private startURL: string = `${this.POKE_API_URL}?offset=0&limit=40`
  private nextPage: string|null = null 
  private prePage: string|null = null

  @Input('isTrainerList')
  set isTrainerList(isTrainerList: boolean) {
    this._isTrainerList = isTrainerList;
  }

  constructor(
    private readonly sessionService: SessionService,
    private readonly pokedexCollection: PokedexCollectionService
  ) { }
  
  //set next an previus url for next and prev 40 pokemon in pokeAPI
  ngOnInit(): void {
    this.nextPage = this.sessionService.getNextPage("Next")
    this.prePage = this.sessionService.getNextPage("Previous")
    this.init();
  }

  //Get pokemon from local storage if possible, else fetch from pokeAPI  
  private init(): void {
    if (this._isTrainerList) {
      this._pokemons = this.sessionService.trainer_pokemons();
    }
    else {
      const pokedex = this.sessionService.getPokedex()
      if (pokedex !== null) {
        this._pokemons = pokedex
      }
      else {
        this.pokedexCollection.fetchPokemon(this.startURL, () => {
          this._pokemons = this.sessionService.getPokedex()
          this.nextPage = this.sessionService.getNextPage("Next")
          this.prePage = this.sessionService.getNextPage("Previous")})
      }
    }
  }
  
  //update after new fetch of pokemon
  public onPageChange(url:string): void{
    if (url !== null){
    this.pokedexCollection.fetchPokemon(url,() => {
      this._pokemons = this.sessionService.getPokedex()
      this.nextPage = this.sessionService.getNextPage("Next")
      this.prePage = this.sessionService.getNextPage("Previous")})
    }
  }

  //get non-deleted pokemon
  get pokemons(): Pokemon[] | null {
    if (this._isTrainerList) {
      let notDeleted = []
      if (this._pokemons != null) {
        for (const pokemon of this._pokemons) {
          if (!pokemon.deleted) {
            notDeleted.push(pokemon)
          }
        }
        return notDeleted;
      }
    }
    return this._pokemons;
  }

  get isTrainerList(): boolean {
    return this._isTrainerList;
  }

  get next(): string | null{
    return this.nextPage
  }

  get prev(): string|null{
    return this.prePage
  }
}
