import { Component, OnInit, Input, ComponentFactoryResolver, Output } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { SessionService } from 'src/app/services/Session.service';

@Component({
  selector: 'app-pokemon-item',
  templateUrl: './pokemon-item.component.html',
  styleUrls: ['./pokemon-item.component.css']
})
export class PokemonItemComponent implements OnInit {
  private _isAdded: boolean = false;
  private isReleased: boolean = false


  @Input() pokemon: Pokemon | undefined;
  @Input() isTrainerList: boolean | undefined;


  constructor(
    private readonly sessionService: SessionService
  ) {
    this.checkOffAdded()
  }

  ngOnInit(): void {
    this.checkOffAdded()
  }

  //Check if pokemon exist in trainer pokemon list. 
  private checkOffAdded(): void {
    if (this.pokemon && !this.isTrainerList) {
      const pokemons = this.sessionService.trainer_pokemons()
      if (pokemons !== null) {
        for (const collectedPokemon of pokemons) {
          if (collectedPokemon.id === this.pokemon.id && !collectedPokemon.deleted) {
            this._isAdded = true;
          }
        }
      }
    }
  }


  // remove pokemon from trainer
  public onReleaseButtonClicked(): void {
    if (this.pokemon) {
      this.isReleased = true
      this.sessionService.removePokemon({ ...this.pokemon });
    }
  }

  public onAddPokemonButtonClicked(): void {
    // add pokemon to trainer
    if (this.pokemon) {
      console.log(this.pokemon)
      this.sessionService.addPokemon({ ...this.pokemon })
    }
    this.checkOffAdded()
  }

  public isAdded(): boolean {
    return this._isAdded;
  }

  public get released() {
    return this.isReleased
  }
}
