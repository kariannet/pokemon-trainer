import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private readonly loginService: LoginService,
    private router: Router
  ) { }

  private animated: boolean = false

  ngOnInit(): void {
  }

  //Activate loading animation
  get animate(): boolean {
    return this.animated
  }

  get user(): User | null {
    return this.loginService.getUser()
  }

  public onSubmit(username: NgForm) {
    this.animated = true
    setTimeout(() => this.loginService.authenticate(username.value.username, () => this.router.navigate(["/catalogue"]))
      , 2000)

  }

}
