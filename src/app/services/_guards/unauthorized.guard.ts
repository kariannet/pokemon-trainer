import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

/**
 * Prevents user from routing to other pages than landing when not logged in
 */
export class UnAuthorized implements CanActivate {
  constructor (private router: Router){}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      const activeSession: string|null = localStorage.getItem('Trainer')
      if (!activeSession){
        return this.router.parseUrl('/')
      }
    return true;
  }
  
}
