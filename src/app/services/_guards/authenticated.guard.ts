import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

/**
 * Prevents routing to landing page when logged in. Redirects to trainer page. 
 */
export class AuthenticatedGuard implements CanActivate {
  constructor (private router: Router){}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      
      const activeSession: string|null = localStorage.getItem('Trainer')
      if (activeSession!== null){
        alert("please log out before you return to login page")
        return this.router.parseUrl("/trainer")
  
  }
  return true;
}
}
