import { TestBed } from '@angular/core/testing';

import { UnAuthorized } from './unauthorized.guard';

describe('AuthGuard', () => {
  let guard: UnAuthorized;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(UnAuthorized);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
