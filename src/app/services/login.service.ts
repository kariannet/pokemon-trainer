import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of} from "rxjs";
import { User } from "src/app/models/user.model";
import { environment } from "src/environments/environment";
import { finalize, switchMap } from 'rxjs/operators';
import { Pokemon } from "../models/pokemon.model";
@Injectable({
    providedIn: 'root'
})


export class LoginService {
    private API_URL: string = environment.USER_API_URL
    private API_KEY: string = environment.USER_API_KEY 
    private _user: User | null= null
    private _error: string = "" 


    constructor (
        private readonly http: HttpClient
        ){}


    //atempts to fetch user from trainerAPI 
    private fetchUser(username: string) : Observable<User[]>{
        return this.http.get<User[]>(`${this.API_URL}?username=${username}`)
       
    }

    //atempts to post user to trainerAPI
    private postUser(username:string): Observable<User>{
        const headers = {'X-API-Key': this.API_KEY, 'Content-Type': 'application/json'}
        const body = {username,pokemon:[]}
        return this.http.post<User>(this.API_URL,body,{headers})
    }

    //Log in if user exist - register and log in if not.
    public authenticate(username: string, onSuccess: () => void): void {    
        this.fetchUser(username)
          .pipe(
            switchMap((users: User[]) => {
              if (users.length) {
                return of(users[0]);
              }
              return this.postUser(username);
            }),
          )
          .subscribe(
              {
                  next:(user: User) => {
                    if (user.id) {
                      this._user = user
                      localStorage.setItem("Trainer", JSON.stringify(this._user))
                      onSuccess();
                    }},
                  error:(error: HttpErrorResponse) => {
                    this._error = error.message;
                  }
              }
          );
      }

      // patch list of pokemons stored in db ( via trainer api)
      public patchPokemon(trainerId:number, pokemons : Pokemon[]): void{
        const headers = {'X-API-Key': this.API_KEY, 'Content-Type': 'application/json'}
        const body = {pokemon:pokemons}
        this.http.patch<User>(`${this.API_URL}/${trainerId}`,body,{headers})
        .subscribe({
          error: (err:HttpErrorResponse) => this._error = err.message 
        })
    }


      public getUser(){
          return this._user
      }

      public getError(){
        return this._error
      }

      public setPokemon(pokemon:Pokemon[]){
        if (this._user){
          this._user.pokemon = pokemon
        }
      }


}

