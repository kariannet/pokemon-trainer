import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { environment } from 'src/environments/environment';
import { SessionService } from './Session.service';

@Injectable({
  providedIn: 'root'
})
export class PokedexCollectionService {
  private prevURL: string = ""
  private _error : string ="" 
  private _pokemons: Pokemon[] = [];
  private imageURL: string =
  'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/';

  constructor(private readonly http: HttpClient,
    private readonly sessionService: SessionService) { }

  public pokemons(): Pokemon[] {
    return this._pokemons;
  }


  /**
   * get a number of pokemon from PokeApi v2 based on url param. Generate pokemon 
   * objects and save to state. Next and previous url give the url to next/previus x number of pokemon.
   *   
   * @param url https://pokeapi.co/api/v2/pokemon?offset= <number to start from> &limit= <number of pokemon>
   * @param onSuccess function to run on success
   */
  public fetchPokemon(url: string, onSuccess: () =>void) {
    this.http.get<any>(url)
      .subscribe(
        {
          next: (pokemons: any) => {
            const newPokemons: Pokemon[] = []
            pokemons.results.map((pokemon: any) => {
              let id = pokemon.url.match(/\/(\d{1,5})\//)[1]
              let newPokemon: Pokemon = {
                id: id,
                indexID: id,
                name: pokemon.name,
                deleted: false,
                avatar: this.imageURL + `${id}.png`
              }
              newPokemons.push(newPokemon)
            }
            )
            
            this._pokemons = [...newPokemons]
            this.sessionService.saveToLocalStorage('Pokedex',this._pokemons)
            this.sessionService.saveToLocalStorage("Previous",pokemons.previous)
            this.sessionService.saveToLocalStorage("Next",pokemons.next)
            onSuccess()
          },
          error: (err:HttpErrorResponse) => this._error = err.message
        }
      )
  }

}

