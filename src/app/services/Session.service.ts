import { Injectable } from "@angular/core";
import { Pokemon } from "../models/pokemon.model";
import { User } from "../models/user.model";
import { LoginService } from "./login.service";

@Injectable({
    providedIn: 'root'
})
export class SessionService {
  

    constructor(private readonly loginService: LoginService) {}


    // Note: should API be updated every time Pokemon are added or deleted, or simply just at 
    // logout?
    /**
     * Adds one pokemon to the trainers pokemon collection.
     * @param pokemon pokemon to add to the collection.
     */
    public addPokemon(pokemon: Pokemon): void {
        const trainer = this.getUser();
        if(trainer){
        const newIndexID = trainer.pokemon.length
        const newPokemon: Pokemon = {
            id: pokemon.id,
            indexID: newIndexID,
            name: pokemon.name,
            avatar: pokemon.avatar,
            deleted: pokemon.deleted
        }
        trainer.pokemon = [...trainer.pokemon,newPokemon]
    
        this.saveToLocalStorage("Trainer", trainer);
        this.loginService.patchPokemon(trainer.id, trainer.pokemon)
    }
    }

    /**
     * Remove a pokemon from the trainer's collection.
     * @param index index of pokemon to remove
     */
    public removePokemon(pokemon: Pokemon): void {
        const trainer = this.getUser()
        if (trainer != null){
        trainer.pokemon[pokemon.indexID].deleted = true;
        this.saveToLocalStorage("Trainer", trainer);
        this.loginService.patchPokemon(trainer.id, trainer.pokemon)
    }}

    /**
     * @returns the trainer's pokemon collection
     */
    public trainer_pokemons(): Pokemon[] | null {
        const trainer = localStorage.getItem("Trainer")
        if (trainer !== null) {
            return JSON.parse(trainer).pokemon;
        }
        return null
    }

    /**
     * 
     * @returns List of pokemon on current pokedex (length 40)
     */
    public getPokedex(): Pokemon[] | null {
        const pokedex = localStorage.getItem("Pokedex")
        if (pokedex !== null) {
            return JSON.parse(pokedex)
        }
        return null
    }

    /**
     * Store the pokemon collection in the local storage.
     */
    public saveToLocalStorage(key: string, data: any): void {
        localStorage.setItem(key, JSON.stringify(data));
    }

    public logout(): void {
        localStorage.clear()
    }

    /**
     * get url for the next pokemon page
     * @param next "Previous" or "Next" 
     * @returns url string 
     */
    public getNextPage(next:string): string | null {
        const url = localStorage.getItem(next)
        if (url !== null) { return JSON.parse(url) 
        }
    return null
}
    /**
     * 
     * @returns User from local storage
     */
    public getUser(): User|null{
        const trainer = localStorage.getItem("Trainer")
        if (trainer !== null){
        return JSON.parse(trainer)
    }
    return null 
}
    
}