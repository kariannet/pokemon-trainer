export interface Pokemon {
    id: number;
    indexID: number;
    name: string;
    avatar: string;
    deleted: boolean;
}