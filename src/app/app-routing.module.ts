import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TrainerPage } from './pages/trainer/trainer.page';
import { LandingPage } from './pages/landing/landing.page';
import { PokemonCataloguePage } from './pages/pokemon-catalogue/pokemon-catalogue.page';
import { UnAuthorized } from './services/_guards/unauthorized.guard';
import { AuthenticatedGuard } from './services/_guards/authenticated.guard';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/landing',
  },
  {
    path: 'landing',
    component: LandingPage,
    canActivate: [AuthenticatedGuard]
  },
  {
    path: 'trainer',
    component: TrainerPage,
    canActivate: [UnAuthorized]
  },
  {
    path: 'catalogue',
    component: PokemonCataloguePage,
    canActivate: [UnAuthorized]

  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
