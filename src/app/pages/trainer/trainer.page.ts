import { Component, OnInit,Inject } from '@angular/core';
import { Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';
import { SessionService } from 'src/app/services/Session.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css'],
})


export class TrainerPage implements OnInit {

  constructor(
    private readonly sessionService: SessionService,
    private router: Router
  ) {}

  ngOnInit(): void {
  }

  public onLogout(): void{
    this.sessionService.logout()
    this.router.navigate(["/"])
  }

}
